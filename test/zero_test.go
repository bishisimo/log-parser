//Author: bishisimo
//Time: 2021/7/8 下午8:28
//Describe:
package test

import (
	"fmt"
	"gitee.com/bishisimo/log-parser/console"
	"gitee.com/bishisimo/log-parser/parser"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"testing"
	"time"
)

func TestZeroLogParser(t *testing.T) {
	fmt.Println(os.Args)
	out := console.NewConsoleWriter(parser.NewZeroLogParser("log-parser"))
	log.Logger = log.Output(out).With().Caller().Timestamp().Logger()
	log.Trace().Msg("trace")
	log.Debug().Msg("debug")
	log.Info().Msg("info")
	log.Warn().Msg("warn")
	log.Error().Msg("error")
	//log.Fatal().Msg("fatal")
}

func TestZeroLog(t *testing.T) {
	out := console.NewConsoleWriter(new(parser.ZeroLogParser))
	Logger := zerolog.New(out).With().Caller().Timestamp().Logger()
	go Logger.Info().Str("key", "v").Msg("1")
	go Logger.Info().Str("key", "v").Msg("2")
	go Logger.Info().Str("key", "v").Msg("3")
	Logger.Info().Str("key", "v").Msg("")
	time.Sleep(time.Second)
}

func BenchmarkZeroLog(b *testing.B) {
	out := console.NewConsoleWriter(new(parser.ZeroLogParser))
	Logger := zerolog.New(out).With().Caller().Timestamp().Logger()
	for i := 0; i < b.N; i++ {
		Logger.Info().Msg("this is a test glog")
	}
}

func TestZeroField(t *testing.T) {
	out := console.NewConsoleWriter(new(parser.ZeroLogParser))
	zerolog.CallerSkipFrameCount = 2 //这里根据实际，另外获取的是Msg调用处的文件路径和行号
	Logger := zerolog.New(out).With().Caller().Timestamp().Logger()
	Logger.Info().Str("mode", "dev").Msg("")
	Logger.Info().Str("key", "v").Msg("")
}
