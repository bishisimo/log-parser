//Author: bishisimo
//Time: 2021/7/12 上午9:46
//Describe:
package model

import "fmt"

const (
	CReset    = 0  //默认
	CBold     = 1  //高亮
	CBlack    = 30 //黑色
	CRed      = 31 //红色
	CGreen    = 32 //绿色
	CYellow   = 33 //黃色
	CBlue     = 34 //蓝色
	CMagenta  = 35 //紫红色
	CCyan     = 36 //青蓝色
	CGray     = 37 //白色
	CDarkGray = 90 //灰色
)

func Colorize(s interface{}, color int, enabled bool) string {
	if !enabled {
		return fmt.Sprintf("%v", s)
	}
	return fmt.Sprintf("\x1b[%dm%v\x1b[0m", color, s)
}
