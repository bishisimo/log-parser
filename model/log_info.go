//Author: bishisimo
//Time: 2021/7/8 下午7:50
//Describe:
package model

type LogInfo struct {
	Lever      string
	Timestamp  int64
	CallerName string
	Location   string
	Fields     map[string]interface{}
	Msg        string
}
